jQuery(function($) {
	
	$('.homepage-slider').slick({
	
	});
	
	$('.gallery-slider').slick({
	
	});
	
	$('.product-slider-slick').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.product-slider-nav'
	});
	$('.product-slider-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.product-slider-slick',
		dots: false,
		centerMode: true,
		focusOnSelect: true
	});
	
	$('.featured-slider').slick({
		  dots: true,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 4,
		  slidesToScroll: 4,
		  responsive: [
			 {
			  breakpoint: 1024,
			  settings: {
			    slidesToShow: 3,
			    slidesToScroll: 3,
			    infinite: true,
			    dots: true
			  }
			},
			{
			  breakpoint: 600,
			  settings: {
			    slidesToShow: 2,
			    slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
			    slidesToShow: 1,
			    slidesToScroll: 1
			  }
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		]
	});
	
	$('#contactSubmit').click(function(e){
		
		// stops default button behaviour
		e.preventDefault();
		
		$.post(
			$('#contact-form').attr("action"), 
			{
				data: $('#contact-form').serialize(),
			},
			function(response){
				if (response.status == 'OK') {
					$("#contactSubmit").hide();
				}
				$(".form-response").html(response.message);
			},
			'json'
		);  
		
		return false;  
	});
	
	$('[data-toggle="popover"]').popover();
	
	/*$('.bundleSelect').ddslick({
	    width: "100%",
	    imagePosition: "left",
	}); */
		
	
});