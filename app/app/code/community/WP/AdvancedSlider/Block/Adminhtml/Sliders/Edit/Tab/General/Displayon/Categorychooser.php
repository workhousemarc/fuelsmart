<?php

class WP_AdvancedSlider_Block_Adminhtml_Sliders_Edit_Tab_General_Displayon_Categorychooser
    extends Mage_Adminhtml_Block_Widget_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('advancedslider/slider_general_categorychooser.phtml');
    }

    protected function _prepareLayout()
    {
        $selected = $this->getData('selected_categories');
        $uniqId = Mage::helper('core')->uniqHash('categories') . 'Tree';
        $categoriesTree = $this->getLayout()
            ->createBlock('advancedslider/adminhtml_widget_categorychooser')
            ->setId($uniqId)
            ->setSelectedCategories($selected);
        $this->setChild('categories', $categoriesTree);
        return parent::_prepareLayout();
    }
}
