<?php

class WP_AdvancedSlider_Block_Adminhtml_Widget_Form_Element_Captioneditor
    extends Varien_Data_Form_Element_Editor
{
    protected function _getButtonsHtml()
    {
        $buttonsHtml = '<div id="buttons'.$this->getHtmlId().'" class="buttons-set">';
        $buttonsHtml.= '<div style="display:none;">' . $this->_getToggleButtonHtml() . '</div>';
        $buttonsHtml.= $this->_getCaptionConstructorButtonHtml($this->isHidden());
        $buttonsHtml.= '<input id="state_'.$this->getHtmlId().'" name="state_'.$this->getHtmlId().'" type="hidden" value="' .
            htmlspecialchars(Mage::getSingleton('adminhtml/session')->getStateCaptionsValue())
            . '" />';
        $buttonsHtml.= '</div>';

        return $buttonsHtml;
    }

    protected function _getCaptionConstructorButtonHtml($visible = true)
    {
        $buttonsHtml = '';

        $url = Mage::getSingleton('adminhtml/url')->getUrl('*/*/captionConstructor');

        $buttonsHtml .= $this->_getButtonHtml(array(
            'title'     => $this->translate('Insert Caption...'),
            'onclick'   => "WpCaptionCreatorPlugin.loadChooser('" . $url . "','" . $this->getHtmlId() . "')",
            'class'     => 'add-variable plugin',
            'style'     => $visible ? '' : 'display:none',
        ));

        return $buttonsHtml;
    }
}
