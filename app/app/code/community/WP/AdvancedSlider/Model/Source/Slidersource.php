<?php

class WP_AdvancedSlider_Model_Source_Slidersource extends Varien_Object
{
    const SOURCE_STANDARD   = 'standard';
    const SOURCE_NICOLE     = 'nicole';
    const SOURCE_KRISTA     = 'krista';
    const SOURCE_XANDRA     = 'xandra';
    const SOURCE_TRISHA     = 'trisha';
    const SOURCE_SAMANTA    = 'samanta';
    const SOURCE_AMANDA     = 'amanda';

    public static function toOptionArray()
    {
        $options[self::SOURCE_STANDARD] = array(
            'value' => self::SOURCE_STANDARD,
            'label' => Mage::helper('advancedslider')->__('Files of Standard Slider')
        );
        $options[self::SOURCE_NICOLE] = array(
            'value' => self::SOURCE_NICOLE,
            'label' => Mage::helper('advancedslider')->__('Files of Nicole Slider')
        );
        $options[self::SOURCE_KRISTA] = array(
            'value' => self::SOURCE_KRISTA,
            'label' => Mage::helper('advancedslider')->__('Files of Krista Slider')
        );
        $options[self::SOURCE_XANDRA] = array(
            'value' => self::SOURCE_XANDRA,
            'label' => Mage::helper('advancedslider')->__('Files of Xandra Slider')
        );
        $options[self::SOURCE_TRISHA] = array(
            'value' => self::SOURCE_TRISHA,
            'label' => Mage::helper('advancedslider')->__('Files of Trisha Slider')
        );
        $options[self::SOURCE_SAMANTA] = array(
            'value' => self::SOURCE_SAMANTA,
            'label' => Mage::helper('advancedslider')->__('Files of Samantha Slider')
        );

        $options[self::SOURCE_AMANDA] = array(
            'value' => self::SOURCE_AMANDA,
            'label' => Mage::helper('advancedslider')->__('Files of Amanda Slider')
        );
        return $options;
    }
}
