<?php

$installer = $this;

$installer->startSetup();

$installer->run("

ALTER TABLE {$installer->getTable('advancedslider_sliders')}
    CHANGE `style` `style` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'standard';

UPDATE {$installer->getTable('advancedslider_sliders')}
    SET `style` = 'standard' WHERE `style` = 'standart';

    ");

$installer->endSetup();
