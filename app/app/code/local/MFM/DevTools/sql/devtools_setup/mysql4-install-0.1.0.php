<?php
$installer = $this;
$installer->startSetup();

#DATABASE PREFIX
$prefix = Mage::getConfig()->getTablePrefix();

// Category Attribute Manager DB Setup
$categoryAttributeManagerSql =
'CREATE TABLE IF NOT EXISTS `'. $prefix .'mfm_devtools_category_attribute` (
  `id` INTEGER NULL AUTO_INCREMENT,
  `attribute_identifier` MEDIUMTEXT NOT NULL,
  `attribute_label` MEDIUMTEXT NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` MEDIUMTEXT NOT NULL,
  PRIMARY KEY (`id`)
);
';

$installer->run($categoryAttributeManagerSql);
$installer->endSetup();
	 