<?php

    class MFM_KoreSeoTools_Model_Observer {

        private $koreSeoToolsConfig;
        private $configExists = false;
        private $wordpressCollection;
        private $generateBlocksAfterObserver;

        /*
         * Function that handles adding <link rel="next" href="" /> and
         * <link rel="prev" href="" /> tags to the header on page load.
         */

        public function __construct() {
            if ($koreSeoToolsConfig = Mage::getStoreConfig('mfm_koreseotools_config')) {
                $this->koreSeoToolsConfig = $koreSeoToolsConfig;
                $this->configExists = true;
            }
        }

        public function addRelationalLinks(Varien_Event_Observer $observer) {

            // Check if the current page is part of our allowed list below as we only want to add links to certain pages.
            $actions = array('catalog_category_view', 'wordpress_index_index', 'wordpress_post_category_view', 'wordpress_archive_view');
            if (!in_array($observer->getAction()->getFullActionName(), $actions)) {
                return;
            }
            // Check if the config data was loaded
            if ($this->configExists) {
                //We use a switch statement to determine which page we are on and run a method specific to that page.
                if ((bool)$this->koreSeoToolsConfig['relational_meta_tags']['enabled'] == true):
                    switch ($observer->getAction()->getFullActionName()) {
                        case "catalog_category_view":
                            ((bool)$this->koreSeoToolsConfig['relational_meta_tags']['enabled'] ? $this->renderCategoryRelationalLinks($observer) : false);
                            break;
                        case "wordpress_index_index":
                            ((bool)$this->koreSeoToolsConfig['relational_meta_tags']['enabled'] ? $this->renderWordpressRelationalLinks($observer) : false);
                            break;
                        case "wordpress_post_category_view":
                            ((bool)$this->koreSeoToolsConfig['relational_meta_tags']['enabled'] ? $this->renderWordpressRelationalLinks($observer) : false);
                            break;
                        case "wordpress_archive_view":
                            ((bool)$this->koreSeoToolsConfig['relational_meta_tags']['enabled'] ? $this->renderWordpressRelationalLinks($observer) : false);
                            break;
                    }
                endif;
            }

        }

        private function renderWordpressRelationalLinks($observer) {

            /* $layout = $observer->getAction()->getLayout();
            $pager = clone $layout->getBlock('wordpress_post_list.pager');
                        $collection = Mage::getResourceModel('wordpress/post_collection');
            $baseLimit = Mage::helper('wordpress')->getWpOption('posts_per_page', 10);

            if (($category = Mage::registry('wordpress_category')) !== null) {
                $collection->addCategoryIdFilter($category->getId());
                $pager->setCollection($collection);
                $pager->setPageVarName('page');
                $pager->setDefaultLimit($baseLimit);
                $pager->setLimit($baseLimit);
                $pager->setAvailableLimit(array($baseLimit => $baseLimit,));
                $pager->setFrameLength(5);
                $lastPage = (int)$pager->getLastPageNum();
            } else {
                $collection->addIsViewableFilter()->addOrder('post_date', 'desc');
                $pager->setCollection($collection);
                $pager->setPageVarName('page');
                $pager->setDefaultLimit($baseLimit);
                $pager->setLimit($baseLimit);
                $pager->setAvailableLimit(array($baseLimit => $baseLimit,));
                $pager->setFrameLength(5);
                $lastPage = (int)$pager->getLastPageNum() - 1;
            }

            $currentPage = (int)Mage::App()->getRequest()->getParam('page');
            $headBlock = $layout->getBlock('head');
            $requestUri = Mage::App()->getRequest()->getRequestUri();
            $uri = preg_replace('/\d+$/', '', $requestUri);

            if ($currentPage == 0) {
                $nextPage = $currentPage + 2;
                $headBlock->addLinkRel('next', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri . '/page/' . $nextPage);
                $headBlock->addLinkRel('canonical', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri);
            } else if ($currentPage == 1) {
                $nextPage = $currentPage + 1;
                $headBlock->addLinkRel('next', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri . $nextPage);
                $headBlock->addLinkRel('canonical', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri . $currentPage);
            } else if ($currentPage == 2) {
                $nextPage = $currentPage + 1;
                $headBlock->addLinkRel('next', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri . $nextPage);
                $prevPage = $currentPage - 1;
                $headBlock->addLinkRel('prev', preg_replace('/\/$/', '', Mage::getBaseUrl()) . preg_replace('/\/page\/\d+$/', '', $requestUri));
                $headBlock->addLinkRel('canonical', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri . $currentPage);
            } else if ($currentPage < $lastPage) {
                $nextPage = $currentPage + 1;
                $headBlock->addLinkRel('next', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri . $nextPage);
                $prevPage = $currentPage - 1;
                $headBlock->addLinkRel('prev', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri . $prevPage);
                $headBlock->addLinkRel('canonical', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri . $currentPage);
            } else {
                $prevPage = $currentPage - 1;
                $headBlock->addLinkRel('prev', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri . $prevPage);
                $headBlock->addLinkRel('canonical', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $uri . $currentPage);
            }
			*/
            return;
	
        }

        private function renderCategoryRelationalLinks($observer) {

            // Get the layout
            $layout = $observer->getAction()->getLayout();
            // Find the head block
            $headBlock = $layout->getBlock('head');
            // Get the toolbar and pager blocks
            $toolbar = clone $layout->getBlock('product_list_toolbar');
            $pager = clone $layout->getBlock('product_list_toolbar_pager');
            // Set the pager properties based on the information we have available
            $pager->setLimitVarName($toolbar->getLimitVarName())
                ->setPageVarName($toolbar->getPageVarName())
                ->setLimit($toolbar->getLimit())
                ->setCollection(Mage::registry('current_category')->getProductCollection());

            $currentPage = (int)Mage::App()->getRequest()->getParam('p');
            $lastPage = (int)$pager->getLastPageNum();

            if ($currentPage == 0) {
                $nextPage = $currentPage + 2;
                $headBlock->addLinkRel('next', preg_replace('/\/$/', '', Mage::registry('current_category')->getUrl()) . '?p=' . $nextPage);
            } else if ($currentPage == 1) {
                $nextPage = $currentPage + 1;
                $headBlock->addLinkRel('next', preg_replace('/\/$/', '', Mage::registry('current_category')->getUrl()) . '?p=' . $nextPage);
            } else if ($currentPage == 2) {
                $nextPage = $currentPage + 1;
                $headBlock->addLinkRel('next', preg_replace('/\/$/', '', Mage::registry('current_category')->getUrl()) . '?p=' . $nextPage);
                $headBlock->addLinkRel('prev', Mage::registry('current_category')->getUrl());
            } else if ($currentPage < $lastPage) {
                $nextPage = $currentPage + 1;
                $headBlock->addLinkRel('next', preg_replace('/\/$/', '', Mage::registry('current_category')->getUrl()) . '?p=' . $nextPage);
                $prevPage = $currentPage - 1;
                $headBlock->addLinkRel('prev', preg_replace('/\/$/', '', Mage::registry('current_category')->getUrl()) . '?p=' . $prevPage);
            } else {
                $prevPage = $currentPage - 1;
                $headBlock->addLinkRel('prev', preg_replace('/\/$/', '', Mage::registry('current_category')->getUrl()) . '?p=' . $prevPage);
            }

        }

        public function addCanonicalLinks(Varien_Event_Observer $observer) {

            // Check if the current page is part of our allowed list below as we only want to add links to certain pages.
            $actions = array('catalog_category_view', 'catalog_product_view', 'wordpress_post_view');
            if (!in_array($observer->getAction()->getFullActionName(), $actions)) {
                return;
            }

            if($this->configExists){
                    switch ($observer->getAction()->getFullActionName()) {
                        case "catalog_category_view":
                            ((bool)$this->koreSeoToolsConfig['canonical_meta_tags']['category_enabled'] ? $this->addCategoryCanonical($observer) : false);
                            break;
                        case "catalog_product_view":
                            ((bool)$this->koreSeoToolsConfig['canonical_meta_tags']['product_enabled'] ? $this->addProductCanonical($observer) : false);
                            break;
                        case "wordpress_post_view":
                            ((bool)$this->koreSeoToolsConfig['canonical_meta_tags']['category_enabled'] ? $this->addPostCanonical($observer) : false);
                            break;
                    }
            }

        }

        private function addPostCanonical($observer) {
            $requestUri = Mage::App()->getRequest()->getRequestUri();
            $layout = $observer->getAction()->getLayout();
            $headBlock = $layout->getBlock('head');
            $headBlock->addLinkRel('canonical', preg_replace('/\/$/', '', Mage::getBaseUrl()) . $requestUri);
        }

        private function addCategoryCanonical($observer) {

            // Get the layout
            $layout = $observer->getAction()->getLayout();
            // Find the head block
            $headBlock = $layout->getBlock('head');
            if (!$headBlock) {
                return;
            }
            // Get the parameter name for pagination
            $p = Mage::getBlockSingleton('page/html_pager')->getPageVarName();
            // Set the rel canonical to the current URL with only the page number
            // parameter (ie remove any filters)
            $headBlock->addLinkRel(
                'canonical', preg_replace('/([^:])(\/{2,})/', '$1/', Mage::getUrl(
                    '*/*/*/', array(
                        '_use_rewrite' => true,
                        '_escape'      => true,
                        '_query'       => array(
                            $p => Mage::app()->getRequest()->getParam($p),
                        ),
                    )
                ))
            );
        }

        private function addProductCanonical($observer) {

            // Get the layout
            $layout = $observer->getAction()->getLayout();
            // Find the head block
            $headBlock = $layout->getBlock('head');
            if (!$headBlock) {
                return;
            }
            $headBlock->removeItem(
                'link_rel', Mage::registry('current_product')->getUrl()
            );
            $headBlock->addLinkRel(
                'canonical', preg_replace('/([^:])(\/{2,})/', '$1/', Mage::getUrl(
                    '*/*/*/', array(
                        '_use_rewrite' => true,
                        '_escape'      => true,
                    )
                ))
            );

        }

    }
