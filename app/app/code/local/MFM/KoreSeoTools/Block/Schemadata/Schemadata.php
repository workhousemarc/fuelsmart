<?php

    class MFM_KoreSeoTools_Block_Schemadata_Schemadata extends Mage_Page_Block_Html_Pager {

        private $koreSeoToolsConfig;
        private $configExists = false;

        public function __construct() {
			Mage::log("test",null,"mfmschema.log");
            if ($koreSeoToolsConfig = Mage::getStoreConfig('mfm_koreseotools_config')):
                $this->koreSeoToolsConfig = $koreSeoToolsConfig;
                $this->configExists = true;				
            endif;
        }

        public function renderSchemaData($type = false) {
			
            if ($type):
                switch ($type):
                    case 'organisation':
                        return $this->renderOrganisationData();
                    case 'place':
                        return $this->renderPlaceData();
                    case 'localbusiness':
                        return $this->renderLocalBusinessData();
                    case 'product':
                        return $this->renderProductData();
                endswitch;
            endif;
        }

        private function renderOrganisationData() {
            if ($this->schemaDataEnabled('schema_data_organisation')):
                $data = '';
				$data .= '
				<script type="application/ld+json">
					{
						"@context": "http://schema.org",
						"@type": "Organization",
						"url": "' . $this->koreSeoToolsConfig['schema_data_organisation']['url'] . '",
						"logo": "' . $this->koreSeoToolsConfig['schema_data_organisation']['logo'] . '",
						"email": "' . $this->koreSeoToolsConfig['schema_data_organisation']['email'] . '",
						"address": {
										"@type": "PostalAddress",
										"addressLocality": "' . $this->koreSeoToolsConfig['schema_data_organisation']['addressLocality'] . '",
										"addressRegion": "' . $this->koreSeoToolsConfig['schema_data_organisation']['addressRegion'] . '",
										"addressCountry":"' . $this->koreSeoToolsConfig['schema_data_organisation']['country'] . '",
										"postalCode": "' . $this->koreSeoToolsConfig['schema_data_organisation']['postalCode'] . '",
										"streetAddress": "' . $this->koreSeoToolsConfig['schema_data_organisation']['streetAddress'] . '"
						},
						"brand": "' . $this->koreSeoToolsConfig['schema_data_organisation']['brand'] . '",
						"telephone": "' . $this->koreSeoToolsConfig['schema_data_organisation']['telephone'] . '",
						"faxNumber": "' . $this->koreSeoToolsConfig['schema_data_organisation']['faxNumber'] . '"
					}
				</script>
				';
				
				
                /*$data .= '<div itemscope itemtype="http://schema.org/Organization">';
                    $data .= '<span itemprop="brand">' . $this->koreSeoToolsConfig['schema_data_organisation']['brand'] . '</span>';
                    $data .= '<span itemprop="url">' . $this->koreSeoToolsConfig['schema_data_organisation']['url'] . '</span>';
                    $data .= '<span itemprop="logo">' . $this->koreSeoToolsConfig['schema_data_organisation']['logo'] . '</span>';
                    $data .= '<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
                        $data .= '<span itemprop="streetAddress">' . $this->koreSeoToolsConfig['schema_data_organisation']['streetAddress'] . '</span>';
                        $data .= '<span itemprop="addressLocality">' . $this->koreSeoToolsConfig['schema_data_organisation']['addressLocality'] . '</span>';
                        $data .= '<span itemprop="addressRegion">' . $this->koreSeoToolsConfig['schema_data_organisation']['addressRegion'] . '</span>';
                        $data .= '<span itemprop="postalCode">' . $this->koreSeoToolsConfig['schema_data_organisation']['postalCode'] . '</span>';
                        $data .= '<span itemprop="addressCountry">' . $this->koreSeoToolsConfig['schema_data_organisation']['country'] . '</span>';
                    $data .= '</div>';
                    $data .= '<div itemprop="contactPoint" itemscope itemtype="http://schema.org/ContactPoint">';
                        $data .= '<span itemprop="contactType">Customer Services</span>';
                        $data .= '<span itemprop="email">' . $this->koreSeoToolsConfig['schema_data_organisation']['email'] . '</span>';
                        $data .= '<span itemprop="telephone">' . $this->koreSeoToolsConfig['schema_data_organisation']['telephone'] . '</span>';
                        $data .= '<span itemprop="faxNumber">' . $this->koreSeoToolsConfig['schema_data_organisation']['faxNumber'] . '</span>';
                    $data .= '</div>';
                $data .= '</div>'; */
				
                return $data;
            endif;

            return;
        }

        private function renderPlaceData() {
            if ($this->schemaDataEnabled('schema_data_place')) :
                $data = '';
				
				$data .= '
				<script type="application/ld+json">
					{
						"@context": "http://schema.org",
						"@type": "Place",            
						"geo": {
										"@type": "GeoCoordinates",
										"latitude": "' . $this->koreSeoToolsConfig['schema_data_place']['latitude'] . '",
										"longitude": "' . $this->koreSeoToolsConfig['schema_data_place']['longitude'] . '"
						},
						"photo": "' . $this->koreSeoToolsConfig['schema_data_place']['photo'] . '"
					}
					</script>				
				';
				
                /*$data .= '<div itemscope itemtype="http://schema.org/Place">';
                    $data .= '<span itemprop="photo">' . $this->koreSeoToolsConfig['schema_data_place']['photo'] . '</span>';
                    $data .= '<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">';
                        $data .= '<span itemprop="latitude">' . $this->koreSeoToolsConfig['schema_data_place']['latitude'] . '</span>';
                        $data .= '<span itemprop="longitude">' . $this->koreSeoToolsConfig['schema_data_place']['longitude'] . '</span>';
                    $data .= '</div>';
                $data .= '</div>'; */
				
				
                return $data;
            endif;
            return;
        }

        private function renderLocalBusinessData() {
            if ($this->schemaDataEnabled('schema_data_business')) :
               					
				
				$data = '';
				
				$data = '
				<script type="application/ld+json">
					{
						"@context": "http://schema.org",
						"@type": "LocalBusiness",
						"name": "' . $this->koreSeoToolsConfig['schema_data_organisation']['brand'] . '",
						"image": "' . $this->koreSeoToolsConfig['schema_data_organisation']['logo'] . '",   
						"openingHoursSpecification": [
							{
							  "@type": "OpeningHoursSpecification", 
							  "closes":  "' . $this->koreSeoToolsConfig['schema_data_business']['sundayclose'] . '",
							  "dayOfWeek": "http://schema.org/Sunday",
							  "opens":  "' . $this->koreSeoToolsConfig['schema_data_business']['sundayopen'] . '"
							},
							{
							  "@type": "OpeningHoursSpecification",
							  "closes": "' . $this->koreSeoToolsConfig['schema_data_business']['saturdayclose'] . '" ,
							  "dayOfWeek": "http://schema.org/Saturday",
							  "opens": "' . $this->koreSeoToolsConfig['schema_data_business']['saturdayopen'] . '"
							},
							{
							  "@type": "OpeningHoursSpecification",
							  "closes":  "' . $this->koreSeoToolsConfig['schema_data_business']['thursdayclose'] . '",
							  "dayOfWeek": "http://schema.org/Thursday",
							  "opens": "' . $this->koreSeoToolsConfig['schema_data_business']['thursdayopen'] . '"
							},
							{
							  "@type": "OpeningHoursSpecification",
							  "closes": "' . $this->koreSeoToolsConfig['schema_data_business']['tuesdayclose'] . '",
							  "dayOfWeek": "http://schema.org/Tuesday",
							  "opens": "' . $this->koreSeoToolsConfig['schema_data_business']['tuesdayopen'] . '"
							},
							{
							  "@type": "OpeningHoursSpecification",
							  "closes": "' . $this->koreSeoToolsConfig['schema_data_business']['fridayclose'] . '",
							  "dayOfWeek":  "http://schema.org/Friday",
							  "opens": "' . $this->koreSeoToolsConfig['schema_data_business']['fridayopen'] . '"
							},
							{
							  "@type": "OpeningHoursSpecification",
							  "closes": "' . $this->koreSeoToolsConfig['schema_data_business']['mondayclose'] . '",
							  "dayOfWeek": "http://schema.org/Monday",
							  "opens": "' . $this->koreSeoToolsConfig['schema_data_business']['mondayopen'] . '"
							},
							{
							  "@type": "OpeningHoursSpecification",
							  "closes": "' . $this->koreSeoToolsConfig['schema_data_business']['wednesdayclose'] . '",
							  "dayOfWeek":  "http://schema.org/Wednesday",
							  "opens": "' . $this->koreSeoToolsConfig['schema_data_business']['wednesdayopen'] . '"
							}
						]
					}
				</script>

				'; 
				
				/*
                $data .= '<div itemscope itemtype="http://schema.org/LocalBusiness">';
                    $data .= '<time itemprop="openingHours" datetime="Mo ' . $this->koreSeoToolsConfig['schema_data_business']['monday'] . '" />';
                    $data .= '<time itemprop="openingHours" datetime="Tu ' . $this->koreSeoToolsConfig['schema_data_business']['tuesday'] . '" />';
                    $data .= '<time itemprop="openingHours" datetime="We ' . $this->koreSeoToolsConfig['schema_data_business']['wednesday'] . '" />';
                    $data .= '<time itemprop="openingHours" datetime="Th ' . $this->koreSeoToolsConfig['schema_data_business']['thursday'] . '" />';
                    $data .= '<time itemprop="openingHours" datetime="Fr ' . $this->koreSeoToolsConfig['schema_data_business']['friday'] . '" />';
                    $data .= '<time itemprop="openingHours" datetime="Sa ' . $this->koreSeoToolsConfig['schema_data_business']['saturday'] . '" />';
                    $data .= '<time itemprop="openingHours" datetime="Su ' . $this->koreSeoToolsConfig['schema_data_business']['sunday'] . '" />';
                $data .= '</div>';
				*/
				
                return $data;
            endif;
            return;
        }

        private function renderProductData(){
			
            if ($this->schemaDataEnabled('schema_data_product')) :
                $product = Mage::registry('current_product');
                $model = Mage::getModel('catalog/product')->load($product->getId());
                $data = '';
                $data .= '<div itemscope itemtype="http://schema.org/Product">';
                $data .= '<span itemprop="name">'.$model->getName().'</span>';
                $data .= '<span itemprop="sku">'.$model->getSku().'</span>';
                $data .= '<span itemprop="description">'.$model->getDescription().'</span>';
                $data .= '<span itemprop="releaseDate"></span>';
                $data .= '<span itemprop="weight"></span>';
                $data .= '<span itemprop="color"></span>';
                $data .= '<span itemprop="image"></span>';
                $data .= '</div>';
                return $data;
            endif;
            return;
        }

        private function schemaDataEnabled($key) {		
			Mage::log($key." - ".(bool)$this->koreSeoToolsConfig[ $key ]['enabled'],null,"schema.log");
            if ((bool)$this->koreSeoToolsConfig[ $key ]['enabled'] == true) {
                return true;
            }
            return false;
        }

    }