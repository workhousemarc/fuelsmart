
<?php class MFM_KoreSeoToolsKoreSeoTools_Block_Page_Html_Pager extends Mage_Page_Block_Html_Pager {

    // Returns URL for specified page in paginated collection
    public function getPagerUrl($params=array()){

        // Remove page number query string if the page being requested is page 1
        if(isset($params[$this->getPageVarName()]) && $params[$this->getPageVarName()] == 1){
            $params[$this->getPageVarName()] = null;
        }
        // Return getPagerUrl from the parent class with our new altered params
        return parent::getPagerUrl($params);
    } 

}